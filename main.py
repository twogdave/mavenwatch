import sys
from datetime import datetime
import time
import os
import subprocess
import traceback
import logging

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from pathlib import Path
from configparser import ConfigParser
from threading import RLock


class OnFileChangeWatch:
    def __init__(self, maven_build_chain):
        self.observers = []
        self.mavenBuildChain = maven_build_chain

    def __register(self):
        for mavenRepoHandler in self.mavenBuildChain:
            observer = Observer()
            observer.schedule(mavenRepoHandler, mavenRepoHandler.path, recursive=True)
            self.observers.append(observer)

    def __start(self):
        for observer in self.observers:
            observer.start()

    def __stop(self):
        for observer in self.observers:
            observer.stop()

    def __join(self):
        for observer in self.observers:
            observer.join()

    def run(self):
        self.__register()
        self.__start()
        try:
            while True:
                for mavenRepoHandler in self.mavenBuildChain:
                    mavenRepoHandler.build()
        except KeyboardInterrupt:
            root_logger.info("mavenWatch stopped!")
        except:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            root_logger.debug(''.join(line for line in lines))
            root_logger.error("mavenWatch stopped with error!")
        finally:
            root_logger.info("stopping and joining observers")
            self.__stop()
            self.__join()
            root_logger.info("done")


def is_file_change_in_target(file_path: Path):
    """
    Checks if the file is the target folder.
    :param: file_path: of type Path to check
    :return: true if is in the target folder otherwise false
    """
    if "target" in str(file_path).split("/"):
        return True  # check if change is in target

    if "target" in str(file_path).split("\\"):
        return True  # check if change is in target

    return False


class MavenBuildHandler(FileSystemEventHandler):

    def __init__(self, file_path, depends_on_repo, extensions, idle_time=5):
        self.path = Path(file_path)
        self.depends_on_repo = depends_on_repo
        self.extensions = extensions
        self.idle_time = idle_time
        self.build_queue = {}
        self.lock = RLock()

    def dispatch(self, event):

        src_path = Path(event.src_path)

        if is_file_change_in_target(src_path):
            return

        if not src_path.exists():
            src_path = self.get_next_existing_directory(str(src_path))

        if src_path is None:
            return

        if self.is_relevant_file_change(src_path):

            pom_xml_parent_path = self.get_nearest_pom_xml(src_path.parent)

            if pom_xml_parent_path is not None:
                root_logger.info("MavenWatch file change detected for file - %s." % src_path)
                root_logger.info("MavenWatch found pom.xml in directory - %s." % pom_xml_parent_path)
                root_logger.info("Adding to build queue")
                self.add(pom_xml_parent_path, src_path)

    def empty(self):
        return len(self.build_queue) == 0

    def add(self, pom_xml_parent_path, src_path):
        pom_xml_path = str(pom_xml_parent_path)
        pom_xml_dir_path = str(pom_xml_parent_path.parent)

        if str(self.path.parent) == pom_xml_dir_path:
            root_logger.info('Top parent reached, skipping {}'.format(pom_xml_path))
            return

        with self.lock:
            paths_to_remove = []

            for build_queue_path in self.build_queue:
                if pom_xml_path != build_queue_path and pom_xml_path in build_queue_path:
                    paths_to_remove.append(build_queue_path)
                    root_logger.info(
                        'Removing {} from to build because {} is one or more level higher allocated.'.format(
                            build_queue_path,
                            pom_xml_path))

            for path_to_remove in paths_to_remove:
                self.build_queue.pop(path_to_remove)

            self.build_queue[pom_xml_path] = {'srcPath': str(src_path), 'addTime': datetime.now()}

    def build_queue_sleep(self):
        sleep_time = self.idle_time

        if len(self.build_queue) > 0:
            sleep_time = (len(self.build_queue) // sleep_time) ** 2

        root_logger.info("Waiting {} seconds for next build Queue - {}".format(sleep_time, str(self.path)))

        time.sleep(sleep_time)

    def show_build_queue(self):
        root_logger.info("Open build queue entries for {} (size={})".format(self.path, len(self.build_queue)))
        with self.lock:
            for file_path, meta_data in self.build_queue.items():
                root_logger.info("key={}, srcPath={}, addTime={}".format(file_path, meta_data['srcPath'],
                                                                         meta_data['addTime'].strftime(
                                                                             "%Y-%m-%d %H:%M:%S")))

    def build(self):
        self.build_queue_sleep()

        if self.depends_on_repo is not None \
                and not self.depends_on_repo.empty():
            root_logger.info(
                "depending repo {} has changes. Waiting! Current repo {}".format(self.depends_on_repo.path, self.path))
            return

        if len(self.build_queue) > 0:
            self.show_build_queue()

        with self.lock:
            to_build_dict = self.build_queue.copy()

        to_remove_from_build_queue = []
        to_rebuild = []

        for file_path, meta_data in to_build_dict.items():
            root_logger.info("Rebuilding project for - % s." % file_path)
            root_logger.info("Source file for - {} (addTime: {}).".format(meta_data['srcPath'],
                                                                          meta_data['addTime'].strftime(
                                                                              "%Y-%m-%d %H:%M:%S")))

            parent = Path(file_path).parent
            result = subprocess.run(maven_build_arguments, cwd=str(parent), capture_output=True, text=True)
            if result.returncode == 1:
                root_logger.error(result.stdout)
                root_logger.error(result.stderr)
                root_logger.error("Could not build project {} (returnCode={}).".format(file_path, result.returncode))
                build_result_logger.error("project={} (returnCode={}).".format(file_path, result.returncode))
                to_rebuild.append(file_path)
            else:
                root_logger.info(result.stdout)
                root_logger.info("Successfully build project {} (returnCode={}).".format(file_path, result.returncode))
                build_result_logger.info("project={} (returnCode={}).".format(file_path, result.returncode))
                to_remove_from_build_queue.append(file_path)

        with self.lock:
            for pom_xml_path in to_remove_from_build_queue:
                root_logger.info("Removing from build queue - successfully build - file: {}".format(pom_xml_path))
                self.build_queue.pop(pom_xml_path)

            for failure_pom_xml_path in to_rebuild:
                failure_parent_pom_xml_path = Path(failure_pom_xml_path).parent

                if failure_parent_pom_xml_path is None:
                    root_logger.info(
                        "Could not find parent of file {}, terminating the build of this file here!".format(
                            failure_pom_xml_path))
                else:
                    root_logger.info(
                        "Adding parent to build because children could not be build - parent {} - file {}".format(
                            failure_parent_pom_xml_path, failure_pom_xml_path))
                    self.add(failure_parent_pom_xml_path, failure_pom_xml_path)

    def is_relevant_file_change(self, file_path: Path):
        """
        Checks if the file is relevant for the build.
        :param: file_path: of type Path to check
        :return: true if is relevant otherwise false
        """
        if not file_path.exists() or file_path.is_dir():
            return True  # File was deleted so the change is relevant and must be build

        file_name, file_extension = os.path.splitext(str(file_path))
        file_extension = file_extension.lower()

        return file_extension.lower() in self.extensions or file_extension.upper() in self.extensions

    def get_next_existing_directory(self, path_to_check):
        """
        Checks if a path exits an if not the next parent in the
        structure that is a valid existing directory is searched and
        returned

        :param: path_to_check: to validate
        :return: a valid existing path
        """
        src_path = Path(path_to_check)

        while not src_path.exists() and src_path != self.path.parent:
            head, tail = os.path.split(str(src_path))
            src_path = Path(head)

        if not src_path.exists():
            return None

        return src_path

    def get_nearest_pom_xml(self, directory):
        """
        Searches the pom.xml file in the given directory.
        If the pom.xml could not be found in this directory.
        Then it is searched in the parent of this directory

        :param: directory: of type pathlib.Path to search for
        :return: the pathlib.Path object to the pom.xml file
        """

        if directory is None:
            return None

        if not directory.exists() or directory.is_file():
            return None

        pom_xml_path = Path(os.path.join(str(directory), "pom.xml"))

        if not pom_xml_path.exists():
            return self.get_nearest_pom_xml(directory.parent)

        return pom_xml_path


# --------------------------------------------------------------------
config = ConfigParser()
config.read('application.properties')

log_path = config.get('logging', 'logging.directory.path')
log_file_name = config.get('logging', 'logging.filename')
logging_level_name = config.get('logging', 'logging.level')
log_format = config.get('logging', 'logging.format', raw=True)

log_build_resul_path = config.get('logging', 'logging.build.result.directory.path')
log_build_result_file_name = config.get('logging', 'logging.build.result.filename')
log_build_result_level_name = config.get('logging', 'logging.build.result.level')
log_build_result_format = config.get('logging', 'logging.build.result.format', raw=True)

maven_directory = config.get('maven', 'maven.directory.path')
maven_settings_file = config.get('maven', 'maven.settings.file.path')
maven_build_arguments = config.get('maven', 'maven.build.command').split()
maven_build_arguments.insert(0, maven_directory + 'bin/mvn.cmd')
maven_build_arguments.append('-s')
maven_build_arguments.append(maven_settings_file)

repos = {}

repo_total_count = -1
for config_entry in config['repo']:

    repo_num_str = config_entry.replace('repo.', '')[0:2]
    if repo_num_str.isnumeric():
        repo_num_int = int(repo_num_str)
        if repo_num_int > repo_total_count:
            repo_total_count = repo_num_int

default_extensions = config.get('repo', 'repo.default.file.extensions.tobuild').split()
default_idle_time = config.get('repo', 'repo.default.idletime', fallback='5')

for repo_num in range(1, repo_total_count + 1):
    repo_ident = 'repo.' + f'{repo_num:02d}'

    path = config.get('repo', repo_ident + '.path')
    extensions = config.get('repo', repo_ident + '.file.extensions.tobuild', fallback=default_extensions)
    depends_on = config.get('repo', repo_ident + '.dependson', fallback=None)
    idle_time = int(config.get('repo', repo_ident + '.idletime', fallback=default_idle_time))

    depends_on_repo = None
    if depends_on is not None:
        depends_on_repo = repos.get(depends_on)

    repos[repo_ident] = MavenBuildHandler(path, depends_on_repo, extensions, idle_time)


def get_logging_level_by_name(name, default=logging.DEBUG):
    if name is None:
        return default
    elif name == 'CRITICAL':
        return logging.CRITICAL
    elif name == 'FATAL':
        return logging.FATAL
    elif name == 'ERROR':
        return logging.ERROR
    elif name == 'WARN':
        return logging.WARNING
    elif name == 'WARNING':
        return logging.WARNING
    elif name == 'INFO':
        return logging.INFO
    elif name == 'DEBUG':
        return logging.DEBUG


def create_logger(logger_name, log_file, with_stream=True,
                  format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
                  level_name='DEBUG'):
    logger = logging.getLogger(logger_name)
    formatter = logging.Formatter(format)

    level = get_logging_level_by_name(level_name)
    logger.setLevel(level)

    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    if with_stream:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    return logger


root_logger = create_logger(logger_name="rootLogger", log_file="{0}/{1}.log".format(log_path, log_file_name),
                            format=log_format,
                            with_stream=True, level_name=logging_level_name)
build_result_logger = create_logger(logger_name="buildResultLogger",
                                    log_file="{0}/{1}.log".format(log_build_resul_path, log_build_result_file_name),
                                    format=log_build_result_format, with_stream=False, level_name=log_build_result_level_name)
# ---------------------------------------------------------------------

if __name__ == '__main__':
    mavenBuildChain = []
    for key, value in repos.items():
        mavenBuildChain.append(value)

    watch = OnFileChangeWatch(mavenBuildChain)
    watch.run()
